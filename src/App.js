import './App.css';
import Button from './components/Button';
import Display from './components/Display';
import Keypad from './components/Keypad';
import React from 'react';

class App extends React.Component {
  constructor() {
    super();
    this.state = { data: ''};
  }

  calculate = () => {
    try {
      // eslint-disable-next-line no-eval
      const result = eval(this.state.data);
      this.setState({ 
        data: result, 
      });
    } catch (err) {
      this.setState({ 
        data: 'enter the values correctly'
      });

      setTimeout( () => {
        this.setState({ 
          data: ''
        });
      }, 1000);
    }
  }

  backspace = () => {
    const res = this.state.data.toString();
    if(res === "enter the values correctly") {
      this.reset();
    } else {
      this.setState({ 
        data: res.slice(0,-1)
      });
    }
  }

  reset = () => {
    this.setState({ 
      data: ""
    });
  }

  handleClick = (event) => {
    const value = event.target.getAttribute('data-value');
    switch(value) {
      case 'AC':
        this.reset();
        break;
      case 'CE':
        this.backspace();
        break;
      case '=':
        this.calculate();
        break;
      default:
        this.setState({ data: this.state.data + value});
    }
  }

  render() {
    return (
      <div>
        <h1>Calculator</h1>
        <div className="Calculator">
          <Display data = {this.state.data} />
          <Keypad>
            <Button onClick={this.handleClick}value="AC" />
            <Button onClick={this.handleClick} value="7" />
            <Button onClick={this.handleClick} value="4" />
            <Button onClick={this.handleClick} value="1" />
            <Button onClick={this.handleClick} value="0" />

            <Button onClick={this.handleClick} value="CE" />
            <Button onClick={this.handleClick} value="8" />
            <Button onClick={this.handleClick} value='5' />
            <Button onClick={this.handleClick} value="2" />
            <Button onClick={this.handleClick} value="." />

            <Button onClick={this.handleClick} value="*" />
            <Button onClick={this.handleClick} value="9" />
            <Button onClick={this.handleClick} value="6" />
            <Button onClick={this.handleClick} value="3" />
            <Button onClick={this.handleClick} value="=" />

            <Button onClick={this.handleClick} value="-" />
            <Button onClick={this.handleClick} size="2" value="+" />
            <Button onClick={this.handleClick} size="2" value="/" />
          </Keypad>
        </div>
      </div>
    );
  }
}


export default App;
